package com.example.likelionhospitalreview2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LikelionHospitalReview2Application {

	public static void main(String[] args) {
		SpringApplication.run(LikelionHospitalReview2Application.class, args);
	}

}
